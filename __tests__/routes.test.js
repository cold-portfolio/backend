const request = require("supertest");
const app = require("../src/app");
describe("Test Routes", () => {
    it("Should test the base route", async () => {
        const res = await request(app)
            .get("/");
        expect(res.statusCode).toEqual(200);
    });
});